# BLOCKSTORM #

Blockstorm is a lighweight Tetris-like game that was created purely for fun and nostalgia. It runs in every HTML5 capable Browser and uses the [Crafty game lib](https://github.com/craftyjs/Crafty).