// Loading scene
// -------------
// Handles the loading of binary assets such as images and audio files
Crafty.scene('Loading', function(){
    // Draw some text for the player to see in case the file
    // takes a noticeable amount of time to load
    Crafty.e('2D, DOM, Text')
        .text('Loading; please wait...')
        .attr({ x: 200, y: 50 })
        .textFont({
            weight: 'bold',
            size: '30px',
            family: 'Arial'
        })
        .textColor('blue');

    // Load our sprite map image
    Crafty.load({
        'images': [ Game.config.spriteMap ],
        'audio': {
            // collect: ['assets/audio/success.mp3']
        }
    }, function() {
        // Once the images are loaded...

        // Define the individual sprites in the image
        // Each one (baby_girl, etc.) becomes a component
        // These components' names are prefixed with "spr_"
        // to remind us that they simply cause the entity
        // to be drawn with a certain sprite
        Crafty.sprite( Game.config.tilesize.source, Game.config.spriteMap, {
            SpriteBlock: [0, 0, 2, 2],
            SpriteBoard: [ 0, 2, 4, 1 ],
            SpriteT: [ 2, 0, 3, 2 ],
            SpriteL: [ 5, 0, 2, 3 ],
            SpriteZ: [ 7, 0, 2, 3 ],


            // composite sprites
            SpriteBlock1: [0, 0],

            SpriteBoard1: [ 0, 2 ],
            SpriteBoard2: [ 1, 2 ],
            SpriteBoard3: [ 3, 2 ],

            SpriteT1: [ 2, 0 ],
            SpriteT2: [ 3, 0 ],
            SpriteT3: [ 4, 0 ],
            SpriteT4: [ 3, 1 ],

            SpriteL1: [ 6, 0 ],
            SpriteL2: [ 6, 1 ],
            SpriteL3: [ 6, 2 ],
            SpriteL4: [ 5, 2 ],

            SpriteZ1: [ 7, 0 ],
            SpriteZ2: [ 8, 1 ],
            SpriteZ3: [ 8, 2 ],
            SpriteZ4: [ 7, 1 ],

            SpriteSolid: [ 0, 3, 3, 3 ]
        });

    });

    Crafty.scene('Inactive');

});

// Inactive scene
// -------------
// Handles the loading of binary assets such as images and audio files
Crafty.scene('Inactive', function() {

    // set Background
    Crafty.background(Game.config.canvas.bg);

    // Draw some text for the player to see in case the file
    // takes a noticeable amount of time to load

    Crafty.e('MenuText')
        .attr({
            y: Game.tilesize * 3,
            h: Game.tilesize * 5
        })
        .text('BLOCKSTORM<br>- Click to start -')
        .textColor('#0000ff')
        .bind('Click', function() {
            Crafty.scene('Game');
        })
        .hover(function() {
            this.css({
                'background': '#eeeeee'
            });
        }, function() {
            this.css({
                'background': '#dddddd'
            });
        });

    Crafty.e('MenuText')
        .attr('y', Game.tilesize * 10)
        .text('Use your Arrow Keys to move, \'ENTER\' to rotate and \'P\' to pause.')
        .textFont({
            'size': Game.tilesize + 'px'
        })

});

// Game scene
// -------------
// Runs the core gameplay loop
Crafty.scene('Game', function() {

    // Register Pause for game
    Crafty.bind('KeyDown', function(e) {
        var pauseText,
            tilesize = Game.tilesize,
            mapTilesize = Game.mapTilesize;

        if(e.key == Crafty.keys.P) {

            Crafty.pause();

            if( Crafty.isPaused() ) {

                pauseText = Crafty.e('2D, DOM, Text')
                    .attr({
                        x: 2 * mapTilesize, y: 3 * mapTilesize,
                        w: 5 * mapTilesize, h: mapTilesize })
                    .text('Pause')
                    .textFont({
                        weight: 'bold',
                        size: tilesize * 2 + 'px',
                        family: 'Arial'
                    })
                    .textColor('#222222')
                    .css({ 'text-align': 'center' });

                // draw manually since EnterFrame stops firing
                pauseText.draw();

                // destroy Text on Unpause
                pauseText.bind('Unpause', function() {
                    this.destroy();
                });
            }
        }

    });
    // end pause

    function buildLandedArray() {
        var dimensions = { x: 15, y: 24 },
            landed = [], row,
            i, j;

        for(i = 0; i < dimensions.y; i++) {
            row = [];
            for(j = 0; j < dimensions.x; j++) {
                row.push(0);
            }
            landed.push(row);
        }

        return landed;
    }

    function pickPart() {
        var parts = [
            'CompositeBlock',
            'CompositeBoard',
            'CompositeT',
            'CompositeL',
            'CompositeZ'
        ];

        return Crafty.math.randomElementOfArray(parts);
    }

    var activeEntity,
        bottomLine = Game.bottomLine,
        landed = buildLandedArray();

    (function drawMap() {
        var map = Game.map,
            width = Game.mapTilesize,
            height= Game.mapTilesize,
            mapDimensions = Game.config.mapDimensions,
            x, y;


        for(x = 0; x < mapDimensions; x++) {
            for(y = 0; y < mapDimensions; y++) {
                if(map[y][x] === '#') {
                    Crafty.e('Solid').attr({
                        x: x * width,
                        y: y * height,
                        w: width,
                        h: height
                    });
                }
            }
        }
    })();

    Crafty.e('ScoreBox').scoreBox('Score: %score<br>Time: %seconds')
        .addComponent('Persist');


    // Callback for Spawning
    Crafty.bind('Spawn', function() {
        var tilesize = Game.tilesize,
            // callback for when entity falls
            onFall = function(old) {

                var hitData,
                    mbr = this.mbr(),
                    distanceToFloor;

                distanceToFloor = bottomLine - (mbr._y + mbr._h);

                // skip if element has not landed
                hitData = this.hitChildren('Landed')
                if(hitData) {
                    // Backtrack
                    this.move('n', tilesize);
                }

                if(hitData || distanceToFloor <= 0 ) {
                    this.trigger('Landed');
                }
            },
            // Callback for after Entity has landed
            onLand = function() {

                // Crafty.audio.play('collect');

                var eliminationRows = [],
                    corridorOffset = Game.corridorOffset,
                    lastEntity, i;

                //  remove component for falling
                this.unbind('Drop')
                    .removeComponent('Falling')
                    .eachChildren( function(parent) {
                        var gridCoords = this.toGridCoords( tilesize, corridorOffset );

                        // place Id into landed Array
                        landed[gridCoords.y][gridCoords.x] = this[0];

                        this.addComponent('Landed');

                    }, this._childComp );

                // eliminate rows if full
                for(i = landed.length - 1; i >= 0; i--) {
                    if(landed[i].indexOf(0) === -1) {
                        eliminationRows.push( i );
                        landed[i].forEach(function(id, index, arr) {
                            var e = Crafty(id), parent;

                            parent = e._parent;
                            e.addComponent('Explosion')
                                .explosion();
                            // store reference to last entity in row;
                            lastEntity = e;

                            arr[index] = 0;

                            if( !parent.hasChildren() ) {
                                parent.destroy();
                            }
                        });
                    }
                }

                if(eliminationRows.length) {
                    // when last animation finishes
                    return lastEntity.bind('AnimationEnd', function() {
                        Crafty.trigger('RowDeletion', {
                            deletedRows: eliminationRows,
                            landed: landed
                        });
                    });
                }

                // if no row is full spawn another piece
                Crafty.trigger('Spawn');

            };

        // spawn new element and start dropping
        activeEntity = Crafty.e( pickPart() )
            .bind('Drop', onFall)
            .bind('Landed', onLand)
            .addComponent('Falling')
            .falling( tilesize );

        if(activeEntity.hitChildren('Landed')) {
            Crafty.scene('GameOver');
        }
    });

    // Callback for Detecting and Deleting Full Rows
    Crafty.bind('RowDeletion', function(deleteData) {
        var tilesize = Game.tilesize,
            corridorOffset = Game.corridorOffset,
            alreadyClustered = [],
            deletedRow = deleteData.deletedRows.slice(-1)[0],
            i, j, landedRow, id, cluster,
            // callback for after clusters closed the gap
            onGapClosed = function() {
                // update positions in landed array
                this.eachEntity(function() {
                    var id = this[0],
                        gridCoords, row, index, i;

                    // take out old entry
                    for(i = 0; i < landed.length; i++) {
                        row = landed[i];
                        index = row.indexOf( id );
                        if(index !== -1) {
                            row[index] = 0;
                        }
                    }

                    // place Id into landed Array
                    gridCoords = this.toGridCoords( tilesize, corridorOffset );
                    landed[gridCoords.y][gridCoords.x] = id;

                });

                // cluster is not needed anymore
                this.destroy();

                // if all Clusters are destroyed
                if(Crafty('Cluster').length === 0) {
                    Crafty.trigger('Spawn');
                }
            };

        /**
         * Clusters is empty
         * alreadyClustered is empty
         * Loop through all entities above elimination row(from bottom to top)
         * Take first entity, init a new cluster and recursively find neighbors
         * If neighbor found that hasn't been found yet => add it to current cluster, add to alreadyClustered
         * If no more neighbors found terminate search for this entity
         * Repeat till all entities have been searched
         */
        function buildCluster(x, y, cluster) {
            var id = landed[y][x], parentId, neighborId;

            // clustered already
            if(alreadyClustered.indexOf(id) !== -1) return false;

            alreadyClustered.push(id);
            cluster.push(id);

            parentId = Crafty(id)._parent[0];

            // search right
            neighborId = landed[y][x + 1];
            if(neighborId > 0 && Crafty(neighborId)._parent[0] === parentId) {
                buildCluster( x + 1, y, cluster );
            }

            // search left
            neighborId = landed[y][x - 1];
            if(neighborId > 0 && Crafty(neighborId)._parent[0] === parentId) {
                buildCluster( x - 1, y, cluster );
            }

            // search up
            neighborId = landed[y - 1][x];
            if(neighborId > 0 && Crafty(neighborId)._parent[0] === parentId) {
                buildCluster( x, y - 1, cluster );
            }

        }

        // include one row below eliminated row if availbale
        (deletedRow > 0) ? deletedRow-- : 0;

        for(i = deletedRow; i >= 0; i--) {
            landedRow = landed[i];
            for(j = 0; j < landedRow.length; j++) {
                id = landedRow[j];
                if(id > 0) {
                    cluster = [];
                    buildCluster(j, i, cluster )
                    if(cluster.length) {
                        Crafty.e('Cluster')
                            .cluster( cluster, tilesize )
                            .bind('GapClosed', onGapClosed);
                    }
                }
            }
        }

    });

    // start the game
    Crafty.trigger('Spawn');


}, function() {
    Crafty('ScoreBox').stopCounting();

	// Remove our event binding from above so that we don't end up having multiple redundant event watchers
	// after multiple restarts of the game
    Crafty.unbind('Spawn');
    Crafty.unbind('KeyDown');
    Crafty.unbind('RowDeletion');

});


// Victory scene
// -------------
// Tells the player when they've won and lets them start a new game
Crafty.scene('Victory', function() {
    var gameElement = document.getElementById('game-container'),
        finishInterval, color, colorArray, rgb, reduction = false, goalColor;

    color = gameElement.style.backgroundColor;

    colorArray = color.match(/\((\d+),\s?(\d+),\s?(\d+)\)/);

    rgb = {
        r: +colorArray[1], g: +colorArray[2], b: +colorArray[3]
    };

    finishInterval = window.setInterval(function() {

        if(!reduction) {

            goalColor = { r: 255, g: 204, b: 17};

            rgb.r = rgb.r.stepTowards(goalColor.r);
            rgb.g = rgb.g.stepTowards(goalColor.g);
            rgb.b = rgb.b.stepTowards(goalColor.b);

            Crafty.background('rgb(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')');

            if(rgb.r == goalColor.r && rgb.g == goalColor.g && rgb.b == goalColor.b) {
                reduction = true;
            }


        } else {

            goalColor = { r: 255, g: 228, b: 225};

            // 255 228	225
            rgb.r = rgb.r.stepTowards(goalColor.r);
            rgb.g = rgb.g.stepTowards(goalColor.g);
            rgb.b = rgb.b.stepTowards(goalColor.b);

            Crafty.background('rgb(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')');

            Crafty.e('2D, DOM, Text')
                .text('... :D!')
                .attr({ x: 200, y: 100 })
                .textFont({
                    color: 'blue',
                    weight: 'bold',
                    size: '30px',
                    family: 'Arial'
                });

            // '#ffcc11'
            if(rgb.r == goalColor.r && rgb.g == goalColor.g && rgb.b == goalColor.b) {

                Crafty.e('Girl');

                window.clearInterval(finishInterval);

                // Crafty.scene('Inactive');
            }

        }

    }, 1000 / 100);

    /*
	// After a short delay, watch for the player to press a key, then restart
	// the game when a key is pressed
    var delay = true;
    setTimeout(function() {
        delay = false;
        if (!delay) {
            Crafty.scene('Inactive');
        }
    }, 5000);
    this.restart_game = function() {
        if (!delay) {
            Crafty.scene('Inactive');
        }
    };
    */
}, function() {
    // Remove our event binding from above so that we don't end up having multiple redundant event watchers
    // after multiple restarts of the game

});

Crafty.scene('GameOver', function() {
    Crafty.background('#dedede');

    Crafty('ScoreBox').removeComponent('Persist');

    Crafty.e('MenuText')
        .attr({
            y: Game.tilesize * 10,
            h: Game.tilesize * 5
        })
        .text('GAME OVER<br>- click to restart -')
        .bind('Click', function() {
            Crafty.scene('Inactive');
        })
        .hover(function() {
            this.css({
                'background': '#eeeeee'
            });
        }, function() {
            this.css({
                'background': '#dddddd'
            });
        });

}, function() {
    // Remove our event binding from above so that we don't end up having multiple redundant event watchers
    // after multiple restarts of the game

});