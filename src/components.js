Crafty.c('Composite', {
    _childComp: 'CompositeChild',
    /**
     * #._initChildren()
     * @desc: Function to initialize children entities. Use #._childMap as Map to set blocks. 0 for no block,
     * Number > 0 for a block with corresponding SpriteNumber.
     */
    _initChildren: function(spriteBaseName) {
        var self = this,
            map = this._childMap || [ [1, 1], [1, 1]],
            tilesize = Game.tilesize;

        map.forEach(function(row, yPos) {
            row.forEach(function(spot, xPos) {
                var child;
                if(spot > 0) {
                    child = Crafty.e(self._childComp + ', ' + spriteBaseName + spot);
                    child.attr( {
                        x: self._x + xPos * tilesize,
                        y: self._y + yPos * tilesize,
                        w: tilesize, h: tilesize
                    } );

                    self.attach(child);
                }
            });
        });

        return this;
    },
    /**
     * Collide children of Composite to a certain Component
     * @param comp: String component name to hit with.
     */
    hitChildren: function(comp) {
        var hitData;

        this.eachChildren(function(parent) {
            if(hitData = this.hit(comp)) {
                return false;
            }
        }, this._childComp);

        return hitData;
    },


    /**
     * Iterate over composite Children, calling a callback in the context of each child.
     * @sign public this .eachChildren(Function callback, String filter)
     * @param callback with parameter parent. 'this' points to the current child. The parent is
     * passed as an additional parameter. If Boolean value false is returned in the callback,
     * the execution will be stopped.
     * @param filter: Optional String Component by which to filter the entities children.
     * Callback will only be executed if Child entity has the component.
     */
    eachChildren: function(callback, filter) {
        var children = this._children,
            i, c, runAgain = true;

        for(i = 0; i < children.length; i++) {
            c = children[i];

            if(filter) {
                if('has' in c && c.has(filter)) {
                    runAgain = callback.call(c, this);
                }
            } else {
                runAgain = callback.call(c, this);
            }

            if(runAgain === false) {
                break;
            }

        }
        return this;
    },
    /**
     * @sign public Boolean .hasChildren(String filter)
     * @param filter: Optional String component by which to filter Children. If filter is passed
     * the function will only return true if at least one child with component 'filter' exists.
     */
    hasChildren: function() {
        var found = false;

        this.eachChildren(function() {
            found = true;
            return false;
        }, this._childComp);
        return found;
    }
})

Crafty.c('CompositeChild', {
    init: function() {
        this.requires('Canvas, Collision');
    },
    /**
     * @sign public Object .toGridCoords(Number tilesize, Object offset)
     * @param tilesize: Size of tiles in pixels.
     * @param offset: Object with properties 'x' and 'y' of Grid offset within Game Element.
     */
    toGridCoords: function(tilesize, offset) {
        var mbr = this.mbr();

        tilesize = tilesize || 16;

        offset = offset || { x: 0, y: 0 };

        return {
            x: (mbr._x - offset.x) / tilesize,
            y: (mbr._y - offset.y) / tilesize
        }
    }

});

Crafty.c('Actor', {
    init: function() {
        var mapTilesize = Game.mapTilesize;
        this.requires( '2D, Canvas, Collision, Color, Composite' )
            .attr( { x: 4 * mapTilesize, y: 0 });
    }

});

Crafty.c('CompositeBlock', {
    init: function() {
        var tilesize = Game.tilesize;
        this.requires( 'Actor' )
            .attr( { w: 2 * tilesize, h: 2 * tilesize })
            .origin('center')
            ._initChildren('SpriteBlock');
    },

    // Map for block positioning. WILL BE SHARED AMONG ENTITIES.
    _childMap: [
        [ 1, 1 ],
        [ 1, 1 ]
    ]

});



Crafty.c('CompositeBoard', {
    init: function() {
        var tilesize = Game.tilesize;
        this.requires('Actor')
            .attr( { w: 4 * tilesize, h: tilesize })
            .origin(1.5 * tilesize , tilesize / 2)
            ._initChildren('SpriteBoard');
    },

    // Map for block positioning. WILL BE SHARED AMONG ENTITIES.
    _childMap: [
        [ 1, 2, 2, 3 ]
    ]

});



Crafty.c('CompositeL', {
    init: function() {
        var tilesize = Game.tilesize;
        this.requires('Actor')
            .attr( { w: 2 * tilesize, h: 3 * tilesize })
            .origin(1.5 * tilesize, 1.5 * tilesize)
            ._initChildren('SpriteL');
    },

    // Map for block positioning. WILL BE SHARED AMONG ENTITIES.
    _childMap: [
        [ 0, 1 ],
        [ 0, 2 ],
        [ 4, 3 ]
    ]

});



Crafty.c('CompositeT', {
    init: function() {
        var tilesize = Game.tilesize;
        this.requires('Actor')
            .attr( { w: 3 * tilesize, h: 2 * tilesize })
            .origin( 1.5 * tilesize, tilesize / 2 )
            ._initChildren('SpriteT');
    },

    // Map for block positioning. WILL BE SHARED AMONG ENTITIES.
    _childMap: [
        [ 1, 2, 3 ],
        [ 0, 4, 0 ]
    ]
});



Crafty.c('CompositeZ', {
    init: function() {
        var tilesize = Game.tilesize;
        this.requires('Actor')
            .attr( { w: 2 * tilesize, h: 3 * tilesize })
            .origin( tilesize / 2, 1.5 * tilesize )
            ._initChildren('SpriteZ');
    },

    // Map for block positioning. WILL BE SHARED AMONG ENTITIES.
    _childMap: [
        [ 1, 0 ],
        [ 4, 2 ],
        [ 0, 3 ]
    ]
});

Crafty.c('Falling', {

    /**
     * Contstuctor to initialze
     * @sign public this .falling(Number speed)
     * @param speed: Speed by which to fall in Pixels.
     */
    falling: function(speed) {
        var tilesize = Game.tilesize;

        // Default Fallspeed
        this._fallSpeed = 4;

        if(speed >= 0) {
            this._fallSpeed = speed;
        }

        this.requires('Controllable')
            .controllable(tilesize, 'Solid', 'Landed')
            .bind('EnterFrame', this._enterFrameFalling);

        return this;

    },

    _enterFrameFalling: function(frameData) {
        var speed = this._fallSpeed,
            frame = 10,
            bottomLine = Game.bottomLine,
            old, mbr;

        // Move closer to bottom
        if(frameData.frame % frame == 0) {
            old = {
                fallSpeed: speed,
                y: this._y
            };
            mbr = this.mbr();
            this.move('s', Math.min(bottomLine - mbr._y - mbr._h, speed))
                .trigger('Drop', old);

        }
    },

    remove: function() {
        this.removeComponent('Controllable')
            .unbind('EnterFrame', this._enterFrameFalling);
    }
});

Crafty.c('Controllable', {
    controllable: function(speed, wallComp, landedComp) {
        // defaultMovespeed
        this._moveSpeed = 16;

        if(speed >= 0) {
            this._moveSpeed = speed;
        }

        this._wallComp = wallComp;
        this._landedComp = landedComp;

        this.requires('Keyboard, Composite')
            .bind('EnterFrame', this._enterFrameControllable)
            .bind('KeyDown', this._rotateAction);

        return this;
    },

    _enterFrameControllable: function(frameData) {
        var frame = 4,
            speed = this._moveSpeed,
            movement = { x: 0, y: 0 },
            hitWall, hitLanded;

        if( !(frameData.frame % frame == 0) ) return false;

        if(this.isDown('LEFT_ARROW')) {
            movement.x -= speed;
        } else if(this.isDown('RIGHT_ARROW')) {
            movement.x += speed;
        } else if(this.isDown('DOWN_ARROW')) {
            movement.y += speed;
        } else if(this.isDown('SPACE')) {
            // Fall to ground
        }

        if(movement.x !== 0) this.x += movement.x;
        if(movement.y !== 0) this.y += movement.y;

        // Stay in bounds and off landed pieces
        if(this._wallComp) {
            hitWall = this.hit( this._wallComp )
        }
        if(this._landedComp) {
            hitLanded = this.hit( this._landedComp );
        }
        if( hitWall || hitLanded ) {
            if(movement.x !== 0) this.x -= movement.x;
            if(movement.y !== 0) this.y -= movement.y;
        }

    },
    _rotateAction: function () {
        var hitWall, hitLanded;

        if(this.isDown('ENTER')) {
            this.rotation = this._rotation + 90;

            // undo rotations into walls or objects
            if(this._wallComp) {
                hitWall = this.hit( this._wallComp )
            }
            if(this._landedComp) {
                hitLanded = this.hit( this._landedComp );
            }
            if( hitWall || hitLanded ) {
                this.rotation = this._rotation - 90;
            }
        };
    },
    remove: function() {
        this.unbind('EnterFrame', this._enterFrameControllable)
            .unbind('KeyDown', this._rotateAction);
    }
});

Crafty.c('Cluster', {

    /**
     * public this .cluster(Array cluster)
     * @param cluster: The ids of the elements to add to cluster.
     */
    cluster: function(cluster, speed) {
        this._entities = cluster;
        this._fallSpeed = speed || 2;

        this.bind('EnterFrame', this._enterFrameCluster);
        return this;
    },

    /**
     * For each cluster move down a tilesize at a time
     * check each entity for hits with Landed
     * check cluster for hit with Solid
     * if collision remove enterFrame
     */
    _enterFrameCluster: function(frameData) {

        var hit = false,
            speed = this._fallSpeed

        // move down each frame and check for hits with Landed or floor
        this.eachEntity(function() {
            var mbr,
                bottomLine = Game.bottomLine,
                distanceToFloor;

            this.move('s', speed);
            mbr = this.mbr();

            distanceToFloor = bottomLine - (mbr._y + mbr._h);

            // getting out of bounds
            if(distanceToFloor < 0) {
                hit = true;
            }

            if(this.hit('Landed')) {
                hit = true;
            }
        });

        // backtrack after hit cluster
        if(hit) {
            this.eachEntity(function() {
                this.move('n', speed);
            });

            // Gap is closed
            this.trigger('GapClosed');
        }

    },

    /**
     * Iterates over each entity which is part of the cluster, calling 'callback' in the contet of each
     * entity
     * @param callback
     */
    eachEntity: function(callback) {
        Crafty.fn.each.call(this._entities, callback);
    },

    remove: function() {
        this.unbind('EnterFrame', this._enterFrameCluster);
    }
});


Crafty.c('Landed', {
    init: function() {
        // do something
    }
});

Crafty.c('Solid', {
    init: function() {
        var mapTilesize = Game.mapTilesize;
        this.requires('2D, Canvas, SpriteSolid')
            .attr( { w: mapTilesize, h: mapTilesize });
    }
});

Crafty.c('MenuText', {
    init: function() {
        var tilesize = Game.tilesize,
            mapTilesize = Game.mapTilesize;
        this.requires('2D, DOM, Text, Hover, Mouse')
            .attr({
                x: mapTilesize, y: mapTilesize,
                w: mapTilesize * 8, h: mapTilesize
            })
            .text('BLOCKSTORM')
            .textFont({
                weight: 'bold',
                size: (tilesize * 2) + 'px'
            })
            .css({
                'border-radius': mapTilesize / 2 + 'px',
                'border': '3px solid black',
                'background': '#dddddd',
                'text-align': 'center',
                'cursor': 'pointer'
            });
    }
});

Crafty.c('Hover', {
    /**
     * Shorthand method to create hover effects using a series of mouse events
     * @sign public this .hover(Function mouseIn, Function mouseOut, [ Function mouseMove ])
     * @param mouseIn: callback for Mouse Entering entity.
     * @param mouseOut: callback for Mouse Leaving entity.
     * @param mouseMove: optional - callback for Mouse Moving while hovering over entity.
     */
    hover: function(mouseIn, mouseOut, mouseMove) {
        this.requires('Mouse');

        // unbind if already bound handlers
        if(this._hoverHandlers) {
            this.stopHover();
        }

        this.bind('MouseOver', mouseIn)
            .bind('MouseOut', mouseOut);

        if(typeof mouseMove == 'function') {
            this.bind('MouseMove', mouseMove);
        }

        // save for unbinding
        this._hoverHandlers = {
            MouseOver : mouseIn,
            MouseOut: mouseOut,
            MouseMove: mouseMove
        };

        return this;
    },
    /**
     * Quick method to unbind all handlers previously bound with .hover().
     * @sign public this .stopHover()
     */
    stopHover: function() {
        var eventType, handlers = this._hoverHandlers;
        for(eventType in handlers) {
            this.unbind(eventType, handlers[eventType]);
        }
        delete this._hoverHandlers;

        return this;
    },
    remove: function() {
        this.stopHover();
    }
});

Crafty.c('ScoreBox', {
    scoreBox: function(text) {
        var tilesize = Game.tilesize,
            mapTilesize = Game.mapTilesize;

        this._score = 0;
        this._startTime = new Date().getTime();
        this._scoreText = text;

        this.requires('2D, DOM, Text')
            .attr({
                x: mapTilesize * 7, y: 10,
                w: mapTilesize * 3, h: mapTilesize
            })
            .css({
                'text-align': 'right'
            })
            .textFont({
                'size': (tilesize * 1.5 )+ 'px'
            })
            .textColor('#ffffff')
            .bind( 'EnterFrame', this._enterFrameScore )
            .bind( 'RowDeletion', this._rowDeletionScore );

        return this;
    },

    stopCounting: function() {
        this.unbind( 'EnterFrame', this._enterFrameScore );
        this.unbind( 'RowDeletion', this._rowDeletionScore );
    },

    _enterFrameScore: function(frameData) {
        var currentTime = new Date().getTime(),
            startTime = this._startTime,
            score = ( '0000' + this._score ).slice(-5),
            passedTime, formatTime;

        passedTime = new Date( currentTime - startTime );
        formatTime = ('0' + passedTime.getMinutes() ).slice(-2) +
            ':' + ('0' + passedTime.getSeconds() ).slice(-2);

        this.text( this._scoreText.replace('%score', score )
            .replace('%seconds', formatTime ) );
    },

    _rowDeletionScore: function(rowData) {
        var rowCount = rowData.deletedRows.length;
        this._score += 100 * rowCount;
    },
    remove: function() {
        this.unbind( 'EnterFrame', this._enterFrameScore );
        this.unbind( 'RowDeletion', this._rowDeletionScore );
    }
});

Crafty.c('Explosion', {
    init: function() {
        this.requires('SpriteAnimation')
            .reel('Explosion', 1000, 0, 6, 8);
    },
    explosion: function() {
        this.animate('Explosion')
            .bind('AnimationEnd', function() {
                this.destroy();
            });
    }
});

/*
// This is the player-controlled character
Crafty.c('PlayerCharacter', {


    // Registers a stop-movement function to be called when
    // this entity hits an entity with the "Solid" component
    stopOnSolids: function() {
        this.onHit('Solid', this.stopMovement);

        return this;
    },

    // Stops the movement
    stopMovement: function(e) {
        this._speed = 0;
        if (this._movement) {
            this.x -= this._movement.x;
            this.y -= this._movement.y;
        }
    }

});
*/